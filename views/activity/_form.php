<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\Category;


/* @var $this yii\web\View */
/* @var $model app\models\Activity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="activity-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

	  
	  <?= (!($model->isNewRecord) ? $form->field($model, 'status_id')->dropDownList(Status::getStatuses()) : '') ?>
	  
	  
	<?/*= $form->field($model, 'status_id')->
				dropDownList(Status::getStatuses()) */?> 

	<?= $form->field($model, 'category_id')->
				dropDownList(Category::getCategory()) ?> 

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
