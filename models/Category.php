<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $name
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }
	
	public static function getCategory()
	{
		$allCategory = self::find()->all();
		$allCategorysArray = ArrayHelper::
					map($allCategory, 'id', 'name');
		return $allCategorysArray;						
	}
	public static function getCategoryWithAllCategory()
	{
		$categorys = self::getCategory();
		$categorys[-1] = 'All categorys';
		$categorys = array_reverse ( $categorys, true );
		return $categorys;	
	}
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
