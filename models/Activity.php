<?php

namespace app\models;

use Yii;
use app\models\Category;

/**
 * This is the model class for table "activity".
 *
 * @property integer $id
 * @property string $title
 * @property integer $category_id
 * @property integer $status_id
 */
class Activity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'title', 'category_id'], 'required'],
            [['id', 'category_id', 'status_id', 'status_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
	 public function getStatusItem()
    {
        return $this->hasOne(Status::className(), ['id' => 'status']);
    }
	public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'category_id' => 'Category ID',
            'status_id' => 'Status ID',
        ];
    }
	 public function beforeSave($insert)
    {
        $return = parent::beforeSave($insert);

        if ($this->isNewRecord)
		    $this->status_id = 2;

        return $return;
    }
}
